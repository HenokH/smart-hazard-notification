namespace HazardsWebAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class approveAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmergencyTitle = c.String(maxLength: 50),
                        EmergencyBody = c.String(maxLength: 250),
                        VenueName = c.String(),
                        CategoryId = c.String(),
                        CategoryName = c.String(),
                        Address = c.String(),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Distance = c.Int(nullable: false),
                        UserId = c.String(),
                        Approved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Posts");
        }
    }
}
