﻿using HazardApproverMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace HazardApproverMVC.Controllers
{
    public class ApproveController : Controller
    {
        // GET: Approve
        public ActionResult Index()
        {
            IEnumerable<Post> postList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("PostAdmin").Result;
            postList = response.Content.ReadAsAsync<IEnumerable<Post>>().Result;
            return View(postList);
        }

        public ActionResult Details(int id)
        {

            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Posts/" + id.ToString()).Result;
            return View(response.Content.ReadAsAsync<Post>().Result);
        }

        [HttpPost]
        public ActionResult Details(Post post)
        {
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Posts/" + post.Id, post).Result;

            return RedirectToAction("Index");
        }


    }
}