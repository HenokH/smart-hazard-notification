﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HazardApproverMVC.Models
{
    public class Hazard
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string EmergencyTitle { get; set; }

        [MaxLength(250)]
        public string EmergencyBody { get; set; }

        public string VenueName { get; set; }

        public string CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string Address { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int Distance { get; set; }

        public string UserId { get; set; }

        public string ApproverId { get; set; }
    }
}