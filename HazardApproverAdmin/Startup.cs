﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HazardApproverMVC.Startup))]
namespace HazardApproverMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
