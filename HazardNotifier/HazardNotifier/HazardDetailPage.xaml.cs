﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HazardNotifier
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HazardDetailPage : ContentPage
	{
		public HazardDetailPage (string EmergencyBody, string Address, string VenueName, double Latitude, double Longitude)
		{
			InitializeComponent ();

            emergencyBody.Text = EmergencyBody;
            address.Text = Address;
            venueName.Text = VenueName;
            latitude.Text = Latitude.ToString();
            longitude.Text = Longitude.ToString();

		}
	}
}