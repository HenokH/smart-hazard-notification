﻿using HazardNotifier.Helpers;
using HazardNotifier.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HazardNotifier
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            var assembly = typeof(MainPage);
            appNameImage.Source = ImageSource.FromResource("HazardNotifier.Assets.Images.appName.png");
            LoginImage.Source = ImageSource.FromResource("HazardNotifier.Assets.Images.login2.png");
        }


        //private async void LoginButton_ClickedAsync(object sender, EventArgs e)
        //{
        //    ApiServices _apiServices = new ApiServices();

        //    string Username = UserNameEntry.Text;
        //    string Password = PasswordEntry.Text;
        //    var accestoken = await _apiServices.LoginAsync(Username, Password);

        //   Settings.AccessToken = accestoken;

        //    if (!string.IsNullOrEmpty(Settings.AccessToken))
        //    {
        //        await Navigation.PushAsync(new HomePage());
        //    }
        //    else
        //    {
        //        await DisplayAlert("error", accestoken, "OK");
        //   }

        //}

        //private void LoginButton_Clicked(object sender, EventArgs e)
        //{
        //    bool IsUserNameEmpty = string.IsNullOrEmpty(UserNameEntry.Text);
        //    bool IsPasswordEmpty = string.IsNullOrEmpty(PasswordEntry.Text);
        //    if (IsUserNameEmpty || IsPasswordEmpty)
        //    {

        //    }
        //    else
        //    {

        //       Navigation.PushAsync(new HomePage());
        //    }

        //}

        private void SignUp_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegistrationPage());
        }
    }
}
