﻿using HazardNotifier.Helpers;
using HazardNotifier.Model;
using HazardNotifier.Services;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace HazardNotifier.ViewModels
{
    public class AddNewPostViewModel
    {
        ApiServices _apiServices = new ApiServices();
        [MaxLength(50)]
        public string EmergencyTitle { get; set; }

        [MaxLength(250)]
        public string EmergencyBody { get; set; }

        public string VenueName { get; set; }

        public string CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string Address { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int Distance { get; set; }

        public ICommand AddCommand
        {
            get
            {
                return new Command(async() => {

                    var post = new Post
                    {
                        EmergencyTitle = EmergencyTitle,
                        EmergencyBody = EmergencyBody,
                        VenueName = VenueName,
                        CategoryId = CategoryId,
                        CategoryName = CategoryName,
                        Address = Address,
                        Latitude =Latitude,
                        Longitude = Longitude,
                        Distance = Distance
                        

                    };
                    await _apiServices.PostPostAsync(post, Settings.AccessToken);
                });
            }
        }

    }
}
