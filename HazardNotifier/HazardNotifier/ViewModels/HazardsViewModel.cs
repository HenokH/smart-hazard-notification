﻿using HazardNotifier.Helpers;
using HazardNotifier.Model;
using HazardNotifier.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace HazardNotifier.ViewModels
{
    public class HazardsViewModel : INotifyPropertyChanged
    {
        ApiServices _apiServices = new ApiServices();
        private List<Hazard> _hazards;

        //public string AccessToken { get; set; }

        public List<Hazard> Hazards
            {
                get { return _hazards; }
                set{
                    _hazards = value;
                    OnPropertyChanged();
                }
            }

        //public ICommand GetHazardsCommand
        //{
        //    get
        //    {
        //        return new Command(async () =>
        //        {
        //            var accesstoken = Settings.AccessToken;
        //            //Hazards = await _apiServices.GetPostsAsync(accesstoken);
        //        });
        //    }
        //}

        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
