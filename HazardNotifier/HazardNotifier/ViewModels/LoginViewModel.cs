﻿using HazardNotifier.Helpers;
using HazardNotifier.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace HazardNotifier.ViewModels
{
    public class LoginViewModel
    {
        private ApiServices _apiServices = new ApiServices();



        public string Username { get; set; }

        public string  Password { get; set; }


        public ICommand LoginCommand
        {
            get
            {
                return new Command(async() =>
                {
                   var accestoken = await  _apiServices.LoginAsync(Username, Password);

                    Settings.AccessToken = accestoken;

                    if (!string.IsNullOrEmpty(Settings.AccessToken))
                    {
                        await Xamarin.Forms.Application.Current.MainPage.Navigation.PushAsync(new HomePage());

                    }
                });
            }

            
        }

        //public LoginViewModel()
        //{
        //    Username = Settings.Username;
        //    Password = Settings.Password;
        //}
    }
}
