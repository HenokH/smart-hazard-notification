﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HazardNotifier.Model
{
    public class Users
    { 
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }


        public Users(string UserName, string Password, string Role)
        {
            this.UserName = UserName;
            this.Password = Password;
            this.Role = Role;

        }

        public Users()
        {

        }

    }

    
}
