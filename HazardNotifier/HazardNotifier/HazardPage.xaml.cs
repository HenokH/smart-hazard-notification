﻿using HazardNotifier.Helpers;
using HazardNotifier.Model;
using HazardNotifier.Services;
using HazardNotifier.ViewModels;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HazardNotifier
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HazardPage : ContentPage
	{
		public HazardPage ()
		{
			InitializeComponent ();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var accesstoken = Settings.AccessToken;

            ApiServices _apiServices = new ApiServices();

            var posts = await _apiServices.GetPostsAsync(accesstoken);

            var ApprovedHazards = posts.FindAll(obj => { return obj.Approved == true; });

            HazardsListView.ItemsSource = ApprovedHazards;
            
        }

        private async void HazardsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var details = e.SelectedItem as Hazard;
            await Navigation.PushAsync(new HazardDetailPage(details.EmergencyBody, details.Address, details.VenueName, details.Latitude,details.Longitude));
        }
    }
}