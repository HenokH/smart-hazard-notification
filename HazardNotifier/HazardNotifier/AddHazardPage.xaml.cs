﻿using HazardNotifier.Helpers;
using HazardNotifier.Logic;
using HazardNotifier.Model;
using HazardNotifier.Services;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HazardNotifier
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddHazardPage : ContentPage
	{
		public AddHazardPage ()
		{
			InitializeComponent ();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var locator = CrossGeolocator.Current;
            var position = await locator.GetPositionAsync();

            var venues = await VenueLogic.GetVenues(position.Latitude, position.Longitude);
            venueListView.ItemsSource = venues;

        }

        private void Cancel_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new HazardPage());
        }

        private async void Save_Clicked(object sender, EventArgs e)
        {
            try
            {
                var selectedVenue = venueListView.SelectedItem as Venue;
                var firstCategory = selectedVenue.categories.FirstOrDefault();
                Post post = new Post()
                {
                    EmergencyTitle = EmergencyTitleEntry.Text,
                    EmergencyBody = EmergencyBodyEntry.Text,
                    CategoryId = firstCategory.id,
                    CategoryName = firstCategory.name,
                    Address = selectedVenue.location.address,
                    Distance = selectedVenue.location.distance,
                    Latitude = selectedVenue.location.lat,
                    Longitude = selectedVenue.location.lng,
                    VenueName = selectedVenue.name

                };


                ApiServices _apiServices = new ApiServices();

                var isSuccess = await _apiServices.PostPostAsync(post, Settings.AccessToken);

                if (isSuccess)
                {
                    await DisplayAlert("Success", "An Emergency was successfully inserted", "OK");

                }
                else
                {
                    await DisplayAlert("Failure", "Unable to insert Emergency", "OK");
                }
            }
            catch (NullReferenceException nre)
            {

            }
            catch (Exception ex)
            {

            }


        }
    }
}
