﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HazardNotifier
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegistrationPage : ContentPage
	{
		public RegistrationPage ()
		{
			InitializeComponent ();
            var assembly = typeof(RegistrationPage);
            TigrayImage.Source = ImageSource.FromResource("HazardNotifier.Assets.Images.LoginIcon.jpg");

            var message = MessageLabel.Text;


            if (message == "User created succesfuly")
            {
                Navigation.PushAsync(new MainPage());

            }

            else
            {

            }
        }
	}
}